import en from "./en-US";
import de from "./de-DE";
import fr from "./fr-CA";
import es from "./es-ES";
import ja from "./ja-JP";

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  ...en,
  ...de,
  ...fr,
  ...es,
  ...ja,
};

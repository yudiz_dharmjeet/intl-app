import { LOCALES } from "../locales";

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  [LOCALES.FRENCH]: {
    "nav.about": "Sur",
    "nav.contact": "Contact",
    "nav.choose": "Choisissez la langue",
    "nav.find": "Trouver un magasin",
    "nav.signin": "S'identifier",
    "nav.join": "Adhérer maintenant",

    "hero.title": "JINGLE TOUT LE CHEMIN AUX FAVORIS GRATUITS",
    "hero.desc":
      "Rejoignez Starbucs® Rewards pour de délicieuses offres et des offres exclusives. ",
    "hero.learn": "Apprendre encore plus",

    "section.title": "NOUVEAU SUR LA LISTE NOCE",
    "section.desc":
      "Pour une touche non laitière à un classique des biscuits des Fêtes, essayez le nouveau latte au lait d'amande et biscuits au sucre glacé.",
    "section.btn": "Commandez maintenant",

    "about.title": "Date et heure actuelles",
    "about.curr": "EUR",
  },
};

import { LOCALES } from "../locales";

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  [LOCALES.ENGLISH]: {
    "nav.about": "About",
    "nav.contact": "Contact",
    "nav.choose": "Choose Langulage",
    "nav.find": "Find a store",
    "nav.signin": "Sign in",
    "nav.join": "Join Now",

    "hero.title": "JINGLE ALL THE WAY TO FREE FAVOURITES",
    "hero.desc":
      "Join Starbucs® Rewards for delicious deals & exclusive offers. ",
    "hero.learn": "Learn more",

    "section.title": "NEW TO THE NOCE LIST",
    "section.desc":
      "For a nondairy twist on a holiday cookie classic, try the new Iced Sugar Cookie Almondmilk Latte.",
    "section.btn": "Order Now",

    "about.title": "Current Date & Time",
    "about.curr": "USD",
  },
};

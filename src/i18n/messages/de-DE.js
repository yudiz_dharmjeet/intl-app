import { LOCALES } from "../locales";

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  [LOCALES.GERMAN]: {
    "nav.about": "Über",
    "nav.contact": "Kontakt",
    "nav.choose": "Wählen Sie Sprache",
    "nav.find": "Finde ein Geschäft",
    "nav.signin": "Anmelden",
    "nav.join": "Jetzt beitreten",

    "hero.title": "JINGLE DEN GANZEN WEG ZU DEN KOSTENFREIEN FAVORITEN",
    "hero.desc":
      "Melden Sie sich bei Starbucs® Rewards an, um köstliche Angebote und exklusive Angebote zu erhalten. ",
    "hero.learn": "Mehr erfahren",

    "section.title": "NEU IN DER NOCE-LISTE",
    "section.desc":
      "Probieren Sie für eine milchfreie Variante eines Weihnachtsplätzchenklassikers den neuen Iced Sugar Cookie Almondmilk Latte.",
    "section.btn": "Jetzt bestellen",

    "about.title": "Aktuelles Datum und Uhrzeit",
    "about.curr": "EUR",
  },
};

import { LOCALES } from "../locales";

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  [LOCALES.SPANISH]: {
    "nav.about": "Sobre",
    "nav.contact": "Contacto",
    "nav.choose": "Elija Idioma",
    "nav.find": "Encuentra una tienda",
    "nav.signin": "Registrarse",
    "nav.join": "Únete ahora",

    "hero.title": "JINGLE TODO EL CAMINO A FAVORITOS GRATIS",
    "hero.desc":
      "Únase a Starbucs® Rewards para disfrutar de deliciosas ofertas y ofertas exclusivas. ",
    "hero.learn": "Aprende más",

    "section.title": "NUEVO EN LA LISTA NOTICIAS",
    "section.desc":
      "Para darle un toque no lácteo a un clásico de las galletas navideñas, pruebe el nuevo Iced Sugar Cookie Almondmilk Latte.",
    "section.btn": "Ordenar ahora",

    "about.title": "Fecha y hora actual",
    "about.curr": "EUR",
  },
};

import { LOCALES } from "../locales";

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  [LOCALES.JAPANESE]: {
    "nav.about": "だいたい",
    "nav.contact": "連絡先",
    "nav.choose": "言語を選択",
    "nav.find": "お店を探す",
    "nav.signin": "サインイン",
    "nav.join": "今すぐ参加",

    "hero.title": "お気に入りを解放するためのすべての方法をジングル",
    "hero.desc":
      "Starbucs®Rewardsに参加して、おいしいお得な情報や特別オファーをご利用ください。 ",
    "hero.learn": "もっと詳しく知る",

    "section.title": "NOCEリストの新機能",
    "section.desc":
      "ホリデークッキーの定番に乳製品以外のひねりを加えるには、新しいアイスシュガークッキーアーモンドミルクラテをお試しください。",
    "section.btn": "今すぐ注文",

    "about.title": "現在の日付と時刻",
    "about.curr": "JPY",
  },
};

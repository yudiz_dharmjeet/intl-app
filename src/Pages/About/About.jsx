import React, { useEffect, useState } from "react";
import { FormattedMessage, FormattedNumber, FormattedTime } from "react-intl";
import { MainState } from "../../context/Context";
// import { MainState } from "../../context/Context";
import translate from "../../i18n/translate";

function About() {
  // const [curr, setCurr] = useState("USD");
  const [value, setValue] = useState(30);

  const { state } = MainState();

  // console.log(state.lang);

  useEffect(() => {
    if (state.lang === "en-us") {
      setValue(30);
    } else if (state.lang === "de-de") {
      setValue(30 * 0.91);
    } else if (state.lang === "fr-ca") {
      setValue(30 * 0.91);
    } else if (state.lang === "es-es") {
      setValue(30 * 0.91);
    } else if (state.lang === "ja-jp") {
      setValue(30 * 120.73);
    }
  }, [state.lang]);

  return (
    <section className="box box-a bg-primary text-center py-md">
      <div className="box-inner">
        <h2 className="text-xl">{translate("about.title")}</h2>

        <p className="text-md">
          Date & Time :{" "}
          <FormattedTime day="numeric" month="long" year="numeric" />
        </p>
        <p className="text-md">
          Currency :{" "}
          <FormattedMessage id="about.curr">
            {(data) => {
              return (
                <FormattedNumber
                  value={value}
                  // eslint-disable-next-line react/style-prop-object
                  style="currency"
                  currency={data}
                />
              );
            }}
          </FormattedMessage>
        </p>
      </div>
    </section>
  );
}

export default About;

import React from "react";
import { Hero, Section } from "../../Components";

import "./Home.scss";

function Home() {
  return (
    <>
      <Hero />
      <Section />
    </>
  );
}

export default Home;

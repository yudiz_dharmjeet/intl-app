import React from "react";
import { Link } from "react-router-dom";
import translate from "../../i18n/translate";

function Hero() {
  return (
    <section className="box box-a bg-primary text-center py-md">
      <div className="box-inner">
        <h2 className="text-xl">{translate("hero.title")}</h2>
        <p className="text-md">
          {translate("hero.desc")}
          <Link to="#">{translate("hero.learn")}</Link>
        </p>
      </div>
    </section>
  );
}

export default Hero;

import React from "react";
import { Link } from "react-router-dom";

import Box from "../../Assets/Images/box-b.png";
import translate from "../../i18n/translate";

function Section() {
  return (
    <section className="box box-b bg-secondary grid-col-2">
      <img src={Box} alt="box" />
      <div className="box-text">
        <h2 className="text-xl">{translate("section.title")}</h2>
        <p className="text-md">{translate("section.desc")}</p>
        <Link to="#" className="btn btn-light-outline">
          {translate("section.btn")}
        </Link>
      </div>
    </section>
  );
}

export default Section;

import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

import "./Navbar.scss";

import Logo from "../../Assets/Images/logo.svg";
import Marker from "../../Assets/Images/marker.svg";
import { MainState } from "../../context/Context";
import { LOCALES } from "../../i18n";
import translate from "../../i18n/translate";

function Navbar() {
  const [lang, setLang] = useState();

  const { dispatch } = MainState(LOCALES.ENGLISH);

  function handleChangeLang(e) {
    setLang(e.target.value);
  }

  useEffect(() => {
    dispatch({ type: "SET_LANG", payload: lang });
  }, [lang, dispatch]);

  return (
    <nav className="navbar">
      <div className="navbar-container">
        <div className="navbar-brand">
          <Link to="/">
            <img src={Logo} alt="Starbucks" />
          </Link>
        </div>
        <ul className="navbar-nav-left">
          <li>
            <Link to="/about">{translate("nav.about")}</Link>
          </li>
        </ul>
        <div className="choose-lang">
          <label htmlFor="lang">{translate("nav.choose")} : </label>

          <select name="lang" id="lang" onChange={handleChangeLang}>
            <option value={LOCALES.ENGLISH}>English</option>
            <option value={LOCALES.GERMAN}>German</option>
            <option value={LOCALES.FRENCH}>French</option>
            <option value={LOCALES.SPANISH}>Spanish</option>
            <option value={LOCALES.JAPANESE}>Japanese</option>
          </select>
        </div>
        <ul className="navbar-nav-right">
          <li>
            <Link to="#">
              <img src={Marker} alt="marker" />
              <span>{translate("nav.find")}</span>
            </Link>
          </li>
          <li>
            <button className="btn btn-dark-outline">
              {translate("nav.signin")}
            </button>
          </li>
          <li>
            <button className="btn btn-dark">{translate("nav.join")}</button>
          </li>
        </ul>
      </div>
    </nav>
  );
}

export default Navbar;

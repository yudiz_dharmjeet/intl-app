import React, { createContext, useContext, useReducer } from "react";

import PropTypes from "prop-types";

import { reducers } from "./Reducers";
import { LOCALES } from "../i18n";

const MainContext = createContext();

function Context({ children }) {
  const [state, dispatch] = useReducer(reducers, {
    lang: LOCALES.ENGLISH,
  });

  return (
    <MainContext.Provider value={{ state, dispatch }}>
      {children}
    </MainContext.Provider>
  );
}

Context.propTypes = {
  children: PropTypes.node,
};

export default Context;

export const MainState = () => {
  return useContext(MainContext);
};

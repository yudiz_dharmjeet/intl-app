import React from "react";

import { BrowserRouter, Route, Routes } from "react-router-dom";
import { Navbar } from "./Components";
import { MainState } from "./context/Context";

import { I18nProvider } from "./i18n";
import { About, Home } from "./Pages";

function App() {
  const { state } = MainState();

  return (
    <>
      <I18nProvider locale={state.lang}>
        <BrowserRouter>
          <Navbar />
          <Routes>
            <Route path="/" exact element={<Home />} />
            <Route path="/about" exact element={<About />} />
          </Routes>
        </BrowserRouter>
      </I18nProvider>
    </>
  );
}

export default App;
